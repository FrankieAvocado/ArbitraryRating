require 'spec_helper'

describe 'Static pages' do

  describe "Home page" do
    it 'should have the right h1' do
      visit '/'
      page.should have_selector('h1', :text => "Welcome to Arbitrary Rating!")
    end
  end
end