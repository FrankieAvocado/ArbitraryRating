require 'spec_helper'

describe 'Igiveit pages' do

  let(:rating) {"3"}
  let(:max) {"5"}
  let(:type) {"gerbils"}

  describe "no rating at all" do
    it 'should tell you that something went wrong' do
      visit '/igiveit'
      page.should have_selector('h1', :text => "something went wrong!")
    end
  end

  describe "rating but no type" do
    it 'should tell you that something went wrong' do
      visit "/igiveit/#{rating}outof#{max}"
      page.should have_selector('h1', :text => "something went wrong!")
    end
  end

  describe "rating and type" do
    it 'should tell you that something went wrong' do
      visit "/igiveit/#{rating}outof#{max}/#{type}"
      page.should have_selector('h1', :text => "This is worth #{rating} out of #{max} #{type}")
    end
  end
end