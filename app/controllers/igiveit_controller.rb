require 'chunky_png'

class RatingResult
  def initialize(rating, type)
      if(rating && type && rating.include?('outof'))
        @rating = Integer(rating.split('outof')[0])
        @max = Integer(rating.split('outof')[1])
        @type = type
      else
        @did_fail = true
      end
  end

  def setImage(url)
    @image_url = url
  end

  def failed?
    return @did_fail
  end

  def rating_value
    return @rating
  end

  def max_value
    return @max
  end

  def type_value
    return @type
  end

  def image_url_value
    return @image_url
  end

  def show_what_we_wanted
    unless @did_fail
      return "This is worth #@rating out of #@max #@type"
    else
      return ""
    end
  end
end

class IgiveitController < ApplicationController

  def xoutofy

    rating = params[:rating]
    type = params[:type]

    @PageRating = RatingResult.new(rating, type)

    unless (@PageRating.failed?)

      color_grey = ChunkyPNG::Color.from_hex('#666666')
      color_black = ChunkyPNG::Color.from_hex('#000000')
      max = @PageRating.max_value
      rate = @PageRating.rating_value
      icon_file = "app/assets/rating_icons/#{@PageRating.type_value}.png"
      grey_icon_file = "app/assets/rating_icons/#{@PageRating.type_value}_.png"

      forceDataReturn = params[:display]

      standard_icon = ChunkyPNG::Image.from_file(icon_file)
      grey_icon = ChunkyPNG::Image.from_file(grey_icon_file)

      png = ChunkyPNG::Image.new(max * 24, 24, ChunkyPNG::Color::TRANSPARENT)
      rate.times do |n|
        #png.circle(n * 24 + 5, 10, 5, color_black, color_black)
        png.compose!(standard_icon, n * 24 + 5, 4)
      end

      if(max > rate)
        difference = max - rate
        difference.times do |n|
          #png.circle((n+rate) * 24 + 5, 10, 5, color_black, color_grey)
          png.compose!(grey_icon, (n+rate) * 24 + 5, 4)
        end
      end

      save_to_name = "#{rate}_out_of_#{max}_#{@PageRating.type_value}.png"
      png.save("public/images/#{save_to_name}")

      unless (forceDataReturn)
        @PageRating.setImage(save_to_name)
        @PageRating
      else
        send_data open("#{Rails.root}/public/images/#{save_to_name}", "rb") {|f| f.read }, :filename => save_to_name, :type => 'image/png'
      end
    else
      @PageRating
    end

  end

end
